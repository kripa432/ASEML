import numpy as np
import keras

def load_dataset():
    X_train = []
    Y_train = []
    X_test = []
    Y_test = []
    
    f = open ("train_pairs.txt", 'r');
    for line in f:
        line = line.replace('(',' ( ');
        line = line.replace(')',' ) ');
        y_train, x_train = line.split(",")
        X_train += [x_train]
        Y_train += [y_train]
    
    f = open("valid_input.txt",'r')
    for line in f:
        line = line.replace('(',' ( ');
        line = line.replace(')',' ) ');
        X_test += [line]
    
    f = open("sample_output.txt", 'r')

    for line in f:
        line = line.replace('(',' ( ');
        line = line.replace(')',' ) ');
        Y_test += [line]
    return X_train,Y_train,X_test,Y_test

def build_vocab(programs):
    vocab = set()
    for program in programs:
        program = program.split()
        vocab = vocab.union(set(program))
    return vocab

def get_word_index(vocab):
    
    word_to_index = {}
    index_to_word ={}
    vocab = list(vocab)
    vocab.sort()
    for idx,word in enumerate(vocab,0):
        word_to_index[word] = idx
        index_to_word[idx] = word

    return word_to_index,index_to_word

def word_to_number(programs,word_to_idx,idx_to_word):
    prog2vec = []
    for program in programs:
        program = program.split()
        prog2vec.append([word_to_idx[word] for word in program])
    return prog2vec
        

def max_seq(programs):
    max_len = 0
    for program in programs:
        max_len = max(max_len,len(program))
    return max_len


#insert <s> in Y_train and Y_test and </s>
def insert_sos(sequences,word_to_idx):
    for i in range (len(sequences)):
        sequences[i]=[word_to_idx['<s>']] + sequences[i]
    return sequences

#padding
def pad_sequence(programs,length,word_to_idx):
    programs= keras.preprocessing.sequence.pad_sequences(programs,
                                                     maxlen=length,
                                                     padding='post',
                                                     truncating='post',
                                                    value=word_to_idx['p'])
    return programs
def write_file(output):
    f = open("kripa432_sample_output.txt","w")
    for line in output:
        f.write(line + '\n')
    f.close()